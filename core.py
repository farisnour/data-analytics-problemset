from __future__ import division

import sys
import json

import math
import pandas as pd
import numpy as np

from itertools import izip, count
import matplotlib.pyplot as plt
from numpy import linspace, loadtxt, ones, convolve
import numpy as np
import pandas as pd
import collections
from random import randint
from matplotlib import style
style.use('fivethirtyeight')
#%matplotlib inline

def main():

	# Make sure you have an up-to-date version of Python
	if sys.version_info <= (2,7):
		raise Exception('You don\'t have an up to date version of Python')

	# Load the CSV file as a Pandas dataframe
	dataframe = pd.read_csv('NzM1MjYwOWUxNTg4.csv')

	# Remove any unnecessary metadata
	dataframe = dataframe.drop([
		'patient_id', 'calories', 'human_id', 'source_created_at', 'source_data', 'source_updated_at'
	], axis=1, errors='ignore')

	#YOUR CODE GOES HERE
	# .......
	#Remove any data points with zeros in duration or distance
	dataframe = dataframe.query('duration != 0 and distance != 0')
	dataframe = dataframe.reset_index(drop=True)

	#This function returns all outliers in our dataset
	def explain_anomalies(y, sigma=1.0):
		""" Helps in exploring the anamolies using stationary standard deviation
		Args:
		-----
			y (pandas.Series): independent variable
			sigma (int): number of standard deviations away from the mean

		Returns:
		--------
			a dict (dict of 'standard_deviation': int, 'anomalies_dict': (index: value))
			containing information about the points indentified as anomalies

		"""
		mean = np.mean(y)
		std = np.std(y)

		# Calculate the variation in the distribution of the residual
		return {'standard_deviation': round(std, 3),
			'anomalies_dict': collections.OrderedDict([(index, y_i,) for index, y_i in izip(count(), y)
				if (abs(y_i-mean) > (sigma*std))])}

	# This function is repsonsible for displaying how the function performs on the given dataset.
	def plot_results(x, y, sigma=1, text_xlabel="X Axis", text_ylabel="Y Axis"):
		""" Helps in generating the plot and flagging the anamolies.
		Supports both moving and stationary standard deviation. Use the 'applying_rolling_std' to switch
		between the two.
		Args:
		-----
			x (pandas.Series): dependent variable
			y (pandas.Series): independent variable
			sigma_value (int): number of standard deviations away from the mean
			text_xlabel (str): label for annotating the X Axis
			text_ylabel (str): label for annotatin the Y Axis
		"""
		plt.figure(figsize=(15, 8))
		plt.plot(x, y, "k.")
		y_av = np.ones(len(y))*np.mean(y)
		plt.plot(x, y_av, color='green')
		plt.xlim(0, len(y))
		plt.xlabel(text_xlabel)
		plt.ylabel(text_ylabel)

		# Query for the anomalies and plot the same
		events = {}
		events = explain_anomalies(y, sigma)

		x_anomaly = np.fromiter(events['anomalies_dict'].iterkeys(), dtype=int, count=len(events['anomalies_dict']))
		y_anomaly = np.fromiter(events['anomalies_dict'].itervalues(), dtype=float, count=len(events['anomalies_dict']))
		plt.plot(x_anomaly, y_anomaly, "r*", markersize=12)

		# add grid and lines and enable the plot
		plt.grid(True)
		plt.show()

	#Plot the data and the outliers for distance as an example
	plot_results(dataframe.index.values, dataframe.distance, sigma=2)

	#Find all outliers individually for each feature independent of other features
	outliers = set([])
	mylist = ['distance', 'duration', 'light', 'moderate', 'sedentary', 'steps', 'total_active_minutes', 'vigorous']

	for feature in mylist:
		[outliers.add(i) for i in explain_anomalies(dataframe[feature], sigma=2)['anomalies_dict'].keys()]
	print outliers

if __name__ == '__main__':
	main()
